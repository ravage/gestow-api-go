package main

import (
    "fmt"
    "g/models"
    "io/ioutil"
    "log"

    "github.com/buaazp/fasthttprouter"
    "github.com/gorilla/schema"
    "github.com/jmoiron/sqlx"
    _ "github.com/lib/pq"
    "github.com/valyala/fasthttp"
    "gopkg.in/yaml.v2"
)

type DatabaseConfig struct {
    Host         string
    Username     string
    Password     string
    Port         int
    Name         string
    SslMode      string
    MaxIdleConns int `yaml:"maxIdleConns"`
    MaxOpenConns int `yaml:"maxOpenConns"`
}

type ServerConfig struct {
    Listen string
    Port   int
}

type Config struct {
    Database DatabaseConfig
    Server   ServerConfig
    Queries  map[string]string
}

func (q *Config) GetQuery(key string) string {
    return q.Queries[key]
}

type App struct {
    db     *sqlx.DB
    config *Config
}

func (app *App) GpsCreateHandler(ctx *fasthttp.RequestCtx) {
    decoder := schema.NewDecoder()
    record := new(models.GPSRecord)
    parsed := make(map[string][]string)

    ctx.PostArgs().VisitAll(func(key, value []byte) {
        parsed[string(key)] = append(parsed[string(key)], string(value))
    })

    decoder.Decode(record, parsed)

    _, err := app.db.NamedExec(app.config.GetQuery("insert"), record)

    if err != nil {
        log.Println(err)
    }

    ctx.WriteString("Done!")
}

func main() {
    config := new(Config)
    configFile, err := ioutil.ReadFile("config.yml")

    if err != nil {
        log.Fatal("Missing queries.yml file")
    }

    yaml.Unmarshal(configFile, config)

    router := fasthttprouter.New()

    connStr := fmt.Sprintf(
        "host=%s user=%s password=%s dbname=%s sslmode=%s",
        config.Database.Host, config.Database.Username,
        config.Database.Password, config.Database.Name,
        config.Database.SslMode)

        db, err := sqlx.Open("postgres", connStr)

        if err != nil {
            log.Fatal(err)
        }

        db.SetMaxIdleConns(config.Database.MaxIdleConns)
        db.SetMaxOpenConns(config.Database.MaxOpenConns)

        defer db.Close()

        app := App{db: db, config: config}

        router.POST("/gps", app.GpsCreateHandler)

        addr := fmt.Sprintf("%s:%d", config.Server.Listen, config.Server.Port)
        fmt.Println("Listening on", addr)
        log.Fatal(fasthttp.ListenAndServe(addr, router.Handler))
    }
