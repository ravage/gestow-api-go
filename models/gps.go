package models

type GPSRecord struct {
	TowRegistry    string  `schema:"towRegistry" db:"towRegistry"`
	Cmd            string  `schema:"cmd" db:"cmd"`
	SerialNumber   string  `schema:"serialNumber" db:"serialNumber"`
	DateTimeInt    int64   `schema:"dateTimeint" db:"dateTimeInt"`
	Validity       int     `schema:"validity" db:"validity"`
	Latitude       float64 `schema:"latitude" db:"latitude"`
	Longitude      float64 `schema:"longitude" db:"longitude"`
	HemisphereLat  string  `schema:"hemisphereLat" db:"hemisphereLat"`
	HemisphereLong string  `schema:"hemisphereLong" db:"hemisphereLong"`
	SatNumber      int     `schema:"satNumber" db:"satNumber"`
	Speed          int     `schema:"speed" db:"speed"`
	Direction      int     `schema:"direction" db:"direction"`
	Baterie        int     `schema:"baterie" db:"baterie"`
	Gsm            int     `schema:"gsm" db:"gsm"`
	Roaming        int     `schema:"roaming" db:"roaming"`
	AccIgnition    int     `schema:"acc_ignition" db:"acc_ignition"`
	Block          int     `schema:"block" db:"block"`
	Input1         int     `schema:"input_1" db:"input_1"`
	Input2         int     `schema:"input_2" db:"input_2"`
	Input3         int     `schema:"input_3" db:"input_3"`
	Input4         int     `schema:"input_4" db:"input_4"`
	Terminal       int     `schema:"terminal" db:"terminal"`
}
